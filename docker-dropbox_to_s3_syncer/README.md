# Dropbox to AWS S3 Syncer

A Docker image that watches a directory of files (typically from Dropbox), and syncs any changes to Amazon S3.

This image assumes the AWS credentials will be passed in as environment variables.

See ../README for how to use.
