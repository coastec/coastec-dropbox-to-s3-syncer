#!/bin/bash -eu
# Syncs a particular directory withing Dropbox to S3. 

CONTENT=/dbox/Dropbox/content.coastec.net.au
CONTENT_MINIMUM_EXPECTED_SIZE=667036
S3_BUCKET=content.coastec.net.au

actualsize="$(du -s $CONTENT | awk '{print $1}')"
if [[ $actualsize -lt $CONTENT_MINIMUM_EXPECTED_SIZE ]]; then
	echo >&2 "Dropbox content not of expected size. Aborting"
	exit 1
fi
echo "`date`: Sync beginning"
aws s3 sync $CONTENT s3://$S3_BUCKET --exclude .dropbox
echo "`date`: Sync over"
