#! /bin/bash
DIRECTORY_TO_OBSERVE="/dbox/Dropbox/content.coastec.net.au"

function log()
{
    echo "`date`:       $*"
}
function block_for_change {
	inotifywait -r \
	    -e modify,move,create,delete \
	    --exclude ".*.swp" \
	    $DIRECTORY_TO_OBSERVE
}
BUILD_SCRIPT="sync_dropbox_to_s3.sh"

function sync {
  bash $BUILD_SCRIPT &
}
set -vx
sync
while block_for_change; do
    log "Sync starting"
    sync
    log "Sync completed"
done
log "$0 finished. This should not normally happen"
